# XTools

## XTools能做什么？

pom.xml版本号生成变量工具；快捷关闭端口(Windows)；代码转化工具；代码预览工具；选择代码可编辑为MarkDown代码片段，快速生成MarkDown文档；

## 参考项目
[XTools插件地址](https://plugins.jetbrains.com/plugin/14400-xtools)

[ConvertYamlAndProperties](https://github.com/chencn/ConvertYamlAndProperties)

[intellij-generateAllSetMethod](https://github.com/gejun123456/intellij-generateAllSetMethod)

## 使用环境

IntelliJ IDEA版（213+)

## 功能说明

1. pom.xml版本号生成变量工具
2. 字段转大小写、驼峰、下滑线、加减引号等
3. 端口关闭工具(Windows)
4. yaml与properties代码互转
5. 选择Bean生成Bean的JSON字符串
6. 预览实体类，字段等
7. 生成对象无默认值的setter方法、有默认值的setter方法、构造器的setter方法、链式调用的setter方法
8. Markdown文档生成工具

## 安装说明

1. IDEA插件商店搜索改名为[XTools](https://plugins.jetbrains.com/plugin/14400-xtools)(原插件名为XHttp)，安装重启即可 
2. XTools-${version}.zip，IDEA导入插件安装包重启

## XHttp使用说明

```  
推荐使用Restful Fast Request，对比XHttp功能更完善，但Restful Fast Request使用收费，如果简单测试也可使用XHttp
2.1.3版本已移除XHttp
```

## XTools工具使用说明

除下方按钮 + 快捷键功能外，还提供以下功能

1. pom.xml版本号生成变量工具
2. 生成对象无默认值的setter方法、有默认值的setter方法、构造器的setter方法、链式调用的setter方法（如实体添加@Accessors、@Builder等注解，点击对象触发提示快捷键Alt+Enter，在候选中生成即可）
3. yaml与properties代码互转（选中文件右键点击转化或XTools窗口P2Y转化）
4. 端口关闭工具


### 快捷键

| 功能              | win快捷键           | 右键点击                              |
|-----------------|------------------|-----------------------------------|
| 版本号生成变量              | Alt + V          | Alt+Enter-> 版本号生成变量                      |
| 预览              | Alt + P          | XTools -> 预览                      |
| 实体类生成JSON字符串    | Alt + I          | XTools -> Bean TO Json            |
| yaml转properties | Alt + Y          | XTools -> Y-TO-P                  |
| properties转yaml | Alt + Shift + Y  | XTools -> P-TO-Y                  |
| 字符串大小写互转        | Alt + U          | XTools -> Upper OR Lower Case     |
| 首字母大小写互转        | Alt + Shift + U  | XTools -> Upper OR Lower First    |
| 单引号转双引号,双引号转单引号 | Alt + 引号         | XTools -> Quotes Cast             |
| 去双引号,加双引号       | Alt + Shift + 引号 | XTools -> Quotes Change           |
| 驼峰命名转下划线并大写     | Alt + Shift + K  | XTools -> Underline And UpperCase |
| 驼峰命名转下划线        | Alt + K          | XTools -> Underline Case          |
| 下划线命名转驼峰        | 无                | XTools -> Camel Case              |
| 创建Markdown笔记    | Alt+M            | XTools -> Create Markdown         |

### 使用截图
- pom.xml版本号生成变量
  ![doc20231222-154737.gif](doc%2Fimg%2Fdoc20231222-154737.gif)
- 创建Markdown文档
  ![创建Markdown文档](https://images.gitee.com/uploads/images/2021/0524/162608_b3171a38_4832857.gif "md.gif")
- yaml与properties互转
  ![yaml与properties互转](https://images.gitee.com/uploads/images/2021/0524/162623_1a19cb3c_4832857.gif "p2y.gif")
- 关闭端口
  ![关闭端口](https://images.gitee.com/uploads/images/2021/0524/162635_2c27e136_4832857.gif "port.gif")

